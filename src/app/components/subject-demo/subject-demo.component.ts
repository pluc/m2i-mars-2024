import { Component, OnInit } from '@angular/core';
import { State, StateService } from '../../services/state.service';
import { Observable } from 'rxjs';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'app-subject-demo',
  standalone: true,
  imports: [AsyncPipe],
  templateUrl: './subject-demo.component.html',
  styleUrl: './subject-demo.component.scss'
})
export class SubjectDemoComponent implements OnInit {

  public currentState$?: Observable<State>;

  constructor(private stateService: StateService) {
  }

  ngOnInit(): void {
    // Récupération de l'observable qui va émettre une 
    // notification à chaque changement de state
    this.currentState$ = this.stateService.state$;
  }

  public incrementState(): void {
    this.stateService.incrementState();
  }
}
