import { Component, OnInit } from '@angular/core';
import { Biere } from '../../entities/biere';
import { FormsModule } from '@angular/forms';
import { BiereService } from '../../services/biere.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { BrasserieService } from '../../services/brasserie.service';
import { Brasserie } from '../../entities/brasserie';
import { BaseEntity } from '../../entities/base-entity';
import { Observable } from 'rxjs';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'app-create-update-biere',
  standalone: true,
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    AsyncPipe
  ],
  templateUrl: './create-update-biere.component.html',
  styleUrl: './create-update-biere.component.scss'
})
export class CreateUpdateBiereComponent implements OnInit {

  public biere?: Biere;

  public dialogRef?: MatDialogRef<CreateUpdateBiereComponent>;

  public brasseries$: Observable<Brasserie[]>;

  constructor(
    private biereService: BiereService,
    private brasserieService: BrasserieService) {
    this.brasseries$ = brasserieService.getAllItems();
  }

  ngOnInit(): void {
    if (this.biere == null) {
      this.biere = new Biere();
    }
  }

  public compareBrasseries(o1: BaseEntity, o2: BaseEntity): boolean {
    return o1?.id === o2?.id;
  }

  public submit(): void {
    if (this.biere == null) {
      return;
    }

    if (this.biere.id == null) {
      this.biereService.create(this.biere);
    } else {
      this.biereService.update(this.biere);
    }

    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
