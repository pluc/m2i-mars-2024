import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../core/components/alert/alert.component';

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [FormsModule, AlertComponent],
  templateUrl: './login-form.component.html',
  styleUrl: './login-form.component.scss'
})
export class LoginFormComponent {

  public login?: string;
  public password?: string;

  public authenticationResult?: boolean;

  constructor(private loginService: LoginService, private router: Router) {
    if (this.loginService.isAuthenticated()) {
      this.router.navigate(['/hello-world']);
    }
  }

  public submit(): void {
    this.loginService.authenticate(this.login, this.password).subscribe(
      (loginSuccess: boolean) => {
        this.authenticationResult = loginSuccess;

        if (loginSuccess) {
          this.router.navigate(['/hello-world']);
        }
      });
  }
}
