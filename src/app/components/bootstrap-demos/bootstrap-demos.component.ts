import { Component } from '@angular/core';

@Component({
  selector: 'app-bootstrap-demos',
  standalone: true,
  imports: [],
  templateUrl: './bootstrap-demos.component.html',
  styleUrl: './bootstrap-demos.component.scss'
})
export class BootstrapDemosComponent {

}
