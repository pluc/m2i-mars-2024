import { BaseEntity } from "./base-entity";

export class TypeDeBiere extends BaseEntity {
    public libelle?: string;
}