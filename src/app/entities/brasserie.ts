import { BaseEntity } from "./base-entity";

export class Brasserie extends BaseEntity {
    public libelle?: string;
    public pays?: string;
}