import { Injectable } from '@angular/core';
import { Brasserie } from '../entities/brasserie';
import { GenericAsyncCRUDService } from './generic-async-crud.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class BrasserieService extends GenericAsyncCRUDService<Brasserie> {

  protected override storageKey: string = "BREWERIES_KEY";
  protected override itemFactory = () => new Brasserie();

  constructor(s: StorageService) {
    super(s);
  }
}
