import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export interface State {
  count: number;
}

@Injectable({
  providedIn: 'root'
})
export class StateService {

  /**
   * Sujet pour émettre un "State"
   */
  private _state$ = new Subject<State>();

  /**
   * Observable correspond au sujet.
   * N'importe qui peut s'abonne à "state$" pour être notifié quand
   * le sujet va émettre.
   */
  public readonly state$: Observable<State> = this._state$.asObservable();

  private _state?: State;

  constructor() { }

  public incrementState(): void {
    const count = this._state?.count || 0;

    // Mise à jour du state
    this._state = {
      count: count + 1
    };

    console.log('current state', this._state);

    // émission du state à mis à jour via le Subject
    this._state$.next(this._state);
  }
}
