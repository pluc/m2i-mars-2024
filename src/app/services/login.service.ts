import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, map, of } from 'rxjs';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private static readonly _tokenStorageKey = "TOKEN_SK";

  /**
   * Le sujet doit être privé pour ne pas être anti-pattern.
   * On est anti-pattern quand le sujet est public car n'importe qui
   * pourra émettre n'importe quand n'importe quoi
   */
  private _isAuthenticated$ = new BehaviorSubject<boolean>(false);

  /**
   * Création d'un observable à partir du sujet privé.
   */
  public isAuthenticated$: Observable<boolean> = this._isAuthenticated$.asObservable();

  private notifyAuthenticationState(loginSuccess: boolean): void {
    this._isAuthenticated$.next(loginSuccess);
  }

  constructor(private storageService: StorageService, private router: Router) {
    this.notifyAuthenticationState(this.isAuthenticated());
  }

  public authenticate(login?: string, password?: string): Observable<boolean> {

    return of(null).pipe(
      map(() => {
        return login == "Toto" && password == "1234"
      }),
      map((loginSuccess: boolean) => {
        if (loginSuccess) {
          this.storageService.set(LoginService._tokenStorageKey, "AUTHENTICATED");
        } else {
          this.storageService.delete(LoginService._tokenStorageKey);
        }

        this.notifyAuthenticationState(loginSuccess);

        return loginSuccess;
      })
    )
  }



  public isAuthenticated(): boolean {
    return this.storageService.get(LoginService._tokenStorageKey) != null;
  }

  public logout(): void {
    // TODO appeler le backend pour le déconnecter proprement
    this.storageService.delete(LoginService._tokenStorageKey);

    this.notifyAuthenticationState(false);

    this.router.navigateByUrl('/login');
  }
}
