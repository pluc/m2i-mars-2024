import { ActivatedRouteSnapshot, CanActivateFn, GuardResult, MaybeAsync, RouterStateSnapshot, Routes } from '@angular/router';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { TypesDeBiereComponent } from './components/types-de-biere/types-de-biere.component';
import { CreateUpdateTypeDeBiereComponent } from './components/create-update-type-de-biere/create-update-type-de-biere.component';
import { BrasseriesComponent } from './components/brasseries/brasseries.component';
import { NewStructuralDirectivesComponent } from './components/new-structural-directives/new-structural-directives.component';
import { inject } from '@angular/core';
import { StorageService } from './services/storage.service';
import { LoginService } from './services/login.service';
import { SubjectDemoComponent } from './components/subject-demo/subject-demo.component';

const canActivatePokemon: CanActivateFn =
    (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): MaybeAsync<GuardResult> => {
        // inject() est une fonction statique pour permettre de s'injecter un service
        const storageService: StorageService = inject(StorageService);
        return storageService.get("pokemon") == "true";
    }

const isAuthenticated: CanActivateFn = () => {
    return inject(LoginService).isAuthenticated();
}

export const routes: Routes = [
    { path: 'subject-demo', component: SubjectDemoComponent },
    {
        path: 'exercices',
        canActivate: [isAuthenticated],
        loadComponent:
            () => import('./components/exercices/exercices.component').then(f => f.ExercicesComponent)
    },
    { path: 'hello-world', canActivate: [isAuthenticated], component: HelloWorldComponent },
    { path: 'types-de-biere', canActivate: [isAuthenticated], component: TypesDeBiereComponent },
    { path: 'create-type-de-biere', canActivate: [isAuthenticated], component: CreateUpdateTypeDeBiereComponent },
    { path: 'update-type-de-biere/:id', canActivate: [isAuthenticated], component: CreateUpdateTypeDeBiereComponent },
    { path: 'brasseries', canActivate: [isAuthenticated], component: BrasseriesComponent },
    {
        path: 'pokedex',
        canActivate: [canActivatePokemon, isAuthenticated],
        loadChildren: () =>
            import('./modules/pokemon/pokemon.module').then(f => f.PokemonModule)
    },
    { path: 'angular-17-features', canActivate: [isAuthenticated], component: NewStructuralDirectivesComponent },
    {
        path: 'bieres', canActivate: [isAuthenticated], loadComponent:
            () => import('./components/bieres/bieres.component').then(f => f.BieresComponent)
    },
    {
        path: '**',
        loadComponent: () => import('./components/login-form/login-form.component').then((f => f.LoginFormComponent))
    }
];


